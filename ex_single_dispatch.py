import json
from functools import singledispatch
from typing import Any, cast

from py_factory import Factory, FactoryProduct


##########################################
# Definition of a serializable interface #
##########################################

class Base(FactoryProduct):
    ...


@singledispatch
def serialize_base(obj: Base) -> Any:
    raise NotImplementedError()


def dump_base(obj: Base) -> str:
    d = {
        "name": type(obj).__name__,
        "data": serialize_base(obj),
    }
    return json.dumps(d)


my_factory = Factory[Base]()


######################################
# Code for implementations of `Base` #
######################################


@my_factory.register
class Foo(Base):

    def __init__(self, num: int):
        self.num = num

    @classmethod
    def fproduct_load(cls, data: Any) -> "Foo":
        return cls(cast(int, data))

    def __repr__(self) -> str:
        return f"Foo({self.num})"


@serialize_base.register
def _(obj: Foo) -> Any:
    return {"num": obj.num}


def load_base(raw: str) -> Base:
    d = json.loads(raw)
    name = d["name"]
    data = d["data"]
    return my_factory(name, data)


#########
# Usage #
#########

def main() -> None:
    f = Foo(123)
    raw = dump_base(f)
    print("raw:", raw)
    g = load_base(raw)
    print("load:", g)


if __name__ == '__main__':
    main()
