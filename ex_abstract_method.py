import json
from abc import abstractmethod
from typing import Any, cast

from py_factory import Factory, FactoryProduct


class Base(FactoryProduct):

    @abstractmethod
    def serialize(self) -> Any:
        ...


my_factory = Factory[Base]()


@my_factory.register
class Foo(Base):

    def __init__(self, num: int):
        self.num = num

    @classmethod
    def fproduct_load(cls, data: Any) -> "Foo":
        return cls(cast(int, data))

    def serialize(self) -> Any:
        return {"num": self.num}

    def __repr__(self) -> str:
        return f"Foo({self.num})"


def dump_base(obj: Base) -> str:
    d = {
        "name": type(obj).__name__,
        "data": obj.serialize(),
    }
    return json.dumps(d)


def load_base(raw: str) -> Base:
    d = json.loads(raw)
    name = d["name"]
    data = d["data"]
    return my_factory(name, data)


def main() -> None:
    f = Foo(123)
    raw = dump_base(f)
    print("raw:", raw)
    g = load_base(raw)
    print("load:", g)


if __name__ == '__main__':
    main()
