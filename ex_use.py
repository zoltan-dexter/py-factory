import json
from abc import ABC, abstractmethod
from typing import Any, Dict, cast, TypeVar, Generic, Type, Optional, Protocol, ClassVar

from py_factory import Factory, FactoryProduct


class Base(FactoryProduct):

    @abstractmethod
    def serialize(self) -> Any:
        ...


class Base2:

    @abstractmethod
    def serialize(self) -> Any:
        ...


my_factory = Factory[Base]()
my_factory2 = Factory[Base2]()


@my_factory.register
class Foo(Base):

    def __init__(self, num: int):
        self.num = num

    @classmethod
    def fproduct_load(cls, data: Any) -> "Foo":
        return cls(cast(int, data))

    def serialize(self) -> Any:
        return {"num": self.num}

    def __repr__(self) -> str:
        return f"Foo({self.num})"


@my_factory2.register
class Foo2(Base2):

    def __init__(self, num: int):
        self.num = num

    @classmethod
    def fproduct_load(cls, data: Any) -> "Foo2":
        return cls(cast(int, data))

    def serialize(self) -> Any:
        return {"num": self.num}

    def __repr__(self) -> str:
        return f"Foo2({self.num})"


def dump_base(obj: Base) -> str:
    d = {
        "name": type(obj).__name__,
        "data": obj.serialize(),
    }
    return json.dumps(d)


def dump_base2(obj: Base2) -> str:
    d = {
        "name": type(obj).__name__,
        "data": obj.serialize(),
    }
    return json.dumps(d)


def load_base(raw: str) -> Base:
    d = json.loads(raw)
    name = d["name"]
    data = d["data"]
    return my_factory(name, data)


def load_base2(raw: str) -> Base2:
    d = json.loads(raw)
    name = d["name"]
    data = d["data"]
    return my_factory2(name, data)


def main() -> None:
    f = Foo(123)
    raw = dump_base(f)
    print("raw:", raw)
    g = load_base(raw)
    print("load:", g)
    print("-" * 80)
    f2 = Foo2(123)
    raw2 = dump_base2(f2)
    print("raw:", raw2)
    g2 = load_base2(raw2)
    print("load:", g2)


if __name__ == '__main__':
    main()
