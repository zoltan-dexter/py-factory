from abc import abstractmethod
from typing import Any, Dict, Generic, Protocol, Type, TypeVar


T = TypeVar("T", covariant=True)


class FactoryProduct(Protocol[T]):

    @classmethod
    @abstractmethod
    def fproduct_load(cls, *args: Any, **kwargs: Any) -> T:
        ...


class Factory(Generic[T]):

    def __init__(self) -> None:
        self._registered: Dict[str, Type[FactoryProduct]] = {}

    def register(self, cls: Type[FactoryProduct]) -> Type[FactoryProduct]:
        self._registered[cls.__name__] = cls
        return cls

    def __call__(self, name: str, *args: Any, **kwargs: Any) -> T:
        return self._registered[name].fproduct_load(*args, **kwargs)
